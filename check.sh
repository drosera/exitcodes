#!/bin/bash

CMD=$1

echo "Checking command \"$CMD\" for success or failure:"
echo ""

$CMD && echo "$CMD was successful" || echo "*** $CMD FAILED ***" 
