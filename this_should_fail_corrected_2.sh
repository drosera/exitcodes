#!/bin/bash

# Ending with a correct exit code 
EXITCODE=0

echo "Starting my workflow..."
if ! ./fail_hidden.sh; then EXITCODE=1; fi
echo "...end ending it"

exit $EXITCODE