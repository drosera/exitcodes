exitcodes (in shellscripts)
=============================

Why is it important to exit a script with a reliable indication of wether it
failed or not (aka "exitcode")?

  * applications, scripts, tools and commands can fail, but
  * you don't always _see_ that they fail, because
    * the software is running in a "silent" mode
    * error messages are discarded by the programmer
    * error messages are hidden in a 3728 line long terminal output or logfile
    * you did't recognize the error messages as such

In automated workflows it might be specially important to make sure, that no processing takes place once a step has failed.  Otherwise you could end up with wrong results or a broken system.

This repository contains a few small demo scripts which demonstrate what can happen if you don't control the outcome of your scripts and how to get back control over the exit code.

  * check.sh: Run as `./check.sh ./one_of_the_scripts` to see if it failed or not
  * fail_visibly.sh: A script which fails visibly
  * fail_hidden.sh: A script which fails w/o any visible error
  * this_should_fail.sh: This script should fail but exits with success ($?=0)
  * this_should_fail_corrected_1.sh: Corrected with "set -e"
  * this_should_fail_corrected_2.sh: Corrected by checking the exit code and exiting immediately
  * this_should_fail_corrected_3.sh: Corrected by checking the exit code and exiting controlled
  * this_should_fail_but_pipe_invalidates_it.sh: Pipe somehow invalidates "set -e"
  * this_should_fail_but_pipe_invalidates_it_corrected.sh: Also fail if pipe fails

---
Project Icon: [rip by Valerie Lamm from the Noun Project](https://thenounproject.com/term/rip/2013874/)
