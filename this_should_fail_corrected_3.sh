#!/bin/bash

# Exiting with a correct exit code when YOU want it 
EXITCODE=0

echo "Starting my workflow..."
if ! ./fail_hidden.sh; then EXITCODE=1; fi
echo "...end ending it"

if [ "$EXITCODE" -ne "0" ]; then
  echo "an error occurred and I want to stop here"
  exit $EXITCODE
fi

exit $EXITCODE